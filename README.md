# Botnet Ips

A list of ips displaying mirai-esque behaviour, thought to be mirai or other malicious botnets / actors

## Origination
All data collected from private sources.

## Why have you published this
So that others may use it for either research or filtering during an attack.
